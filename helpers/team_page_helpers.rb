module TeamPageHelpers
  def team_sisense_chart(team_id:, dashboard_id:, chart_id:, html_options: nil)
    html_options ||= { width: '100%', height: 300 }
    src = signed_periscope_url(
      dashboard: dashboard_id,
      chart: chart_id,
      embed: 'v2',
      filters: [{ name: 'team_id', value: team_id }]
    )

    tag(:embed, html_options.merge(src: src))
  end

  def team_mr_rate_chart(team_id:, html_options: nil)
    team_sisense_chart(
      team_id: team_id,
      dashboard_id: 570334,
      chart_id: 7432827,
      html_options: html_options
    )
  end

  def team_mean_time_to_merge_chart(team_id:, html_options: nil)
    team_sisense_chart(
      team_id: team_id,
      dashboard_id: 570334,
      chart_id: 7433451,
      html_options: html_options
    )
  end

  def team_mr_categories_chart(team_id:, html_options: nil)
    team_sisense_chart(
      team_id: team_id,
      dashboard_id: 570334,
      chart_id: 7432830,
      html_options: html_options
    )
  end
end

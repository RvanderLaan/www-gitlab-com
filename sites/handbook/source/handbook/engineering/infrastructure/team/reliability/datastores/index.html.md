---
layout: handbook-page-toc
title: "Datastores Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who We Are

The Datastores team owns our persistent storage platforms, namely our PostgreSQL databases (our main priority) and our Gitaly backend service.

PostgreSQL Databases we look after in Gitlab:
- Staging cluster.
- Production cluster.
- Archive and Delayed production replicas.
- Cloud SQL cluster in ops.gitlab.net
- Prefect Cloud SQL databases, staging and production clusters.

Other components we look after as part of the Database ecosystem in Gitlab:
- Patroni templates.
- Consul (running on the Database clusters)
- PG Bouncer connection pooler.


Main Gitaly components we look after, in production and staging:
- Repo File servers
- Praefect (Gitaly Cluster)
- Gitaly Rails App and nodes

Datastores is:

| Person | Role |
| ------ | ------ |
|[Alberto Ramos](/company/team/#albertoramos)|[Engineering Manager, Reliability](https://about.gitlab.com/job-families/engineering/engineering-management-infrastructure/#engineering-manager-reliability)|
|[Alejandro Rodríguez](/company/team/#eReGeBe)|[Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Ahmad Sherif](/company/team/#ahmadsherif)|[Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Henri Philipps](/company/team/#hphilipps)|[Senior Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|[Jose Cores Finotto](/company/team/#jose-finotto)|[Staff Database Reliability Engineer](/job-families/engineering/database-reliability-engineer/)|
|[Nels Nelson](/company/team/#nnelson)|[Site Reliability Engineer](/job-families/engineering/site-reliability-engineer/)|
|Open Position|[Database Reliability Engineer](/job-families/engineering/database-reliability-engineer/)|


## Vision

*WIP*

## Tenets

*WIP*

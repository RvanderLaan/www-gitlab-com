---
layout: handbook-page-toc
title: Product Team Performance Indicators
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles) - [**Processes**](/handbook/product/product-processes) - [**Categorization**](/handbook/product/product-categories) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## Structure

In order to provide a useful single metric for [product groups](https://about.gitlab.com/company/team/structure/#product-groups) which maps well to product-wide Key Performance indicators, some product performance indicators cascade upwards. Here is how:

1. [Action Monthly Active Users (AMAU)](#action-monthly-active-users-amau) - These are unique users using a feature or viewing a page. _For example_ - unique users interacting with a [feature flag](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)
1. [Group Monthly Active Users (GMAU)](#group-monthly-active-users-gmau) - Are used for targeting the R&D efforts of [Product Groups](/company/team/structure/#product-groups). These are the maximum from the set of the group's AMAUs. _For example_ - In the [Progressive Delivery group](/handbook/product/product-categories/#progressive-delivery-group), if [feature flag](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html) AMAU was 9,000 users and [review apps](https://docs.gitlab.com/ee/ci/review_apps/) AMAU was 11,000 then the Progressive Delivery GMAU would be 11,000.
1. [Stage Monthly Active users (SMAU)](#stage-monthly-active-users-smau) - Are used, in part, for targeting [R&D investments](/handbook/product/investment/#investment-by-stage) across multiple stages. These are the maximum from the set of the relevant GMAU's plus any stage level AMAU's that might not fall cleanly into a GMAU. _For example_ - In the [Release stage](/handbook/product/product-categories/#release-stage), if Release:Progressive Delivery GMAU is 11,000 and Release:Release Management GMAU is 8,000 and the Release-wide AMAU of unique users performing a deployment is 20,000 then the Release SMAU is 20,000.
1. [Section Monthly Active users (Section MAU)](#section-monthly-active-users-smau) - Are used, in part, for targeting [R&D investments](/handbook/product/investment/#investment-by-stage) across multiple sections. These are the maximum from the set of the relevant SMAU's plus any section level AMAU's that might not fall cleanly into a SMAU. _For example_ - The [Secure and Protect section](/handbook/product/secure-and-protect-section-performance-indicators), has opted to use an AMAU of [Unique users who have used a Secure scanner](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/#secure-and-protect-section---section-mau---unique-users-who-have-used-a-secure-scanner) to represent usage in their section.
1. [Section Total Monthly Active Users (Section TMAU)](#section-total-monthly-active-users-section-tmau) - Is used to highlight stage expansion across a section by summing the SMAUs across a section. Unlike other MAU metrics, each user will be counted multiple times.
1. [Total Monthly Active Users (TMAU)](#total-monthly-active-users-tmau) - Is used to highlight stage expansion by summing the SMAUs across all stages. Unlike other MAU metrics, each user will be counted multiple times. This is ok, as the metric is intended to capture the number of [Unique Monthly Active Users](https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau) multiplied by [Stages per User](https://about.gitlab.com/handbook/product/performance-indicators/#stages-per-user-spu).
1. [Unique Monthly Active Users (UMAU)](#unique-monthly-active-users-umau) - Are unique users across the entire product.

_Notes:_

- For GMAU and SMAU there is a desire for these metrics to be the unique users across the aggregate of associated AMAU's but current limitations in how we receive usage ping data (already aggregated per event) means that is not currently possible. There is work to [create de-duplicated GMAU and SMAU metrics in Usage Ping underway](https://gitlab.com/gitlab-org/product-analytics/-/issues/421).

### Non-MAU Performance Indicators

1. Primary Performance Indicator (PPI) - For some sections, stages, or groups, a MAU-style metric may not be their primary performance indicator (PPI). In these scenarios, a different metric will be marked as the PPI to indicate it is the primary metric.

### Three Versions of xMAU

xMAU is a single term to capture the various levels at which we capture Monthly Active Usage (MAU), encompassing Action (AMAU), Group (GMAU), Stage (SMAU), and Total (TMAU).  If you can use GMAU instead of xMAU please do so because it is more specific, more actionable, and a leading indicator. We should not give xMAU metrics without clearly indicating one of the three versions, Reported, Estimated, or Projected.

Each xMAU metric should have three versions:

1. Reported = reported .com xMAU + reported self-managed xMAU
1. Estimated = reported .com xMAU + estimated self-managed xMAU. The purpose of the estimated metric is to account for the fact that only part of self-managed instances report back xMAU data. The estimation is methodology is described on the [is it any good page](https://about.gitlab.com/is-it-any-good/#gitlab-is-the-most-used-devops-tool)
1. Projected = (reported .com xMAU + estimated self-managed xMAU) * (1 + current MoM growth rate * 36 months). The purpose of the projected metric is to gain a sense for what usage could look like in 3 years at current growth rates.

<%= performance_indicators('Product') %>

## Product Metrics Guide

Please see [Product Metrics Guide](/direction/product-analytics#product-metrics-guide)

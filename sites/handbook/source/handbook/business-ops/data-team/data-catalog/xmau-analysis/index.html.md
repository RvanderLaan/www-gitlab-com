---
layout: handbook-page-toc
title: "MAU/TMAU/Section MAU/SMAU/GMAU/AMAU Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## MAU/TMAU/Section MAU/SMAU/GMAU/AMAU Analysis

MAU/TMAU/Section MAU/SMAU/GMAU/AMAU is a single term to capture the various levels at which we capture Monthly Active Usage (MAU), encompassing Action (AMAU), Group (GMAU), Stage (SMAU), and Total (TMAU). In order to provide a useful single metric for product groups which maps well to product-wide Key Performance indicators, each MAU/TMAU/Section MAU/SMAU/GMAU/AMAU metric cascades upwards in the order noted above. 

MAU/TMAU/Section MAU/SMAU/GMAU/AMAU metrics are derived from Usage Ping (Self-Managed instance-level granularity) and GitLab.com (SaaS namespace-level granularity). This Analytics Workflow enables the analysis of each level of MAU/TMAU/Section MAU/SMAU/GMAU/AMAU metric across various segments of customers sets the foundation for reporting on [Reported, Estimated, and Projected](https://about.gitlab.com/handbook/product/performance-indicators/#three-versions-of-MAU/TMAU/Section MAU/SMAU/GMAU/AMAU) metrics.

### Knowledge Assessment & Certification

`Coming Soon`

### Data Classification

Some data supporting MAU/TMAU/Section MAU/SMAU/GMAU/AMAU Analysis is classified as [Orange](/handbook/engineering/security/data-classification-standard.html#orange) or [Yellow](/handbook/engineering/security/data-classification-standard.html#yellow). This includes ORANGE customer metadata from the account, contact data from Salesforce and Zuora and GitLab's Non public financial information, all of which shouldn't be publicly available. Care should be taken when sharing data from this dashboard to ensure that the detail stays within GitLab as an organization and that appropriate approvals are given for any external sharing. In addition, when working with row or record level customer metadata care should always be taken to avoid saving any data on personal devices or laptops. This data should remain in [Snowflake](/handbook/business-ops/data-team/platform/#data-warehouse) and [Sisense](/handbook/business-ops/data-team/platform/periscope/) and should ideally be shared only through those applications unless otherwise approved. 

**ORANGE**

- Description: Customer and Personal data at the row or record level.
- Objects:
  - `dim_billing_accounts`
  - `dim_crm_accounts`
  - `usage_ping_mart`

### Solution Ownership

- Source System Owner:
    - Versions: `@jeromezng`
    - Gitlab.com: `TBD`
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Source System Subject Matter Expert:
    - Versions: `@jeromezng`
    - Gitlab.com: `TBD`
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Data Team Subject Matter Expert: `@mpeychet_` `@m_walker`

### Key Terms

- [Account](/handbook/sales/#additional-customer-definitions-for-internal-reporting)
- Account Instances - the total number of reported instances that can be mapped to an account
- [Host](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)
- [Instance](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)
- Instance User Count - the total number of users on an instance
- [Paid User](/handbook/product/performance-indicators/#paid-user)
- [Product Tier](/handbook/marketing/strategic-marketing/tiers/#overview)
- [Usage Ping](https://docs.gitlab.com/ee/development/telemetry/event_dictionary.html)
- [Version](/handbook/sales/process/version-check/#what-is-the-functionality-of-the-gitlab-version-check)

### Key Metrics, KPIs, and PIs

Explanations for the metrics below can be found on [the Product Team Performance Indicator page](/handbook/product/performance-indicators/#structure):
- [Action Monthly Active Users (AMAU)](/handbook/product/performance-indicators/#action-monthly-active-users-amau)
- [Stage Monthly Active Users (SMAU)](/handbook/product/performance-indicators/#stage-monthly-active-users-smau)
- [Section Monthly Active Users (Section MAU)](/handbook/product/performance-indicators/#structure)
- [Section Total Monthly Active Users (Section TMAU)](https://about.gitlab.com/handbook/product/performance-indicators/#structure)
- [Total Monthly Active Users (TMAU)](/handbook/product/performance-indicators/#structure)

Each metric has three different versions (Recorded, Estimated, Projected), explained on
  - [the Product Team Performance Indicator page](/handbook/product/performance-indicators/#three-versions-of-xmau) 
  - [the Sisense Style Guide](/handbook/business-ops/data-team/platform/sisense-style-guide/#recorded-and-calculated-data)
Currently, recorded metrics that have identified usage ping metrics have been charted on the Centralized Dashboard, but we are working on our first version of Estimated values [in this issue](https://gitlab.com/gitlab-data/analytics/-/issues/6547#note_429610192).

#### Going from Recorded to Estimated xMAU

For the Self-Managed instance that hosts our SaaS GitLab.com product, we get the accurate value via Usage Pings. For other Self-Managed instances, customers have the option to turn disable Usage Ping; therefore, we would not receive any usage statistics for that instance. In order to calculate estimated usage values across our Self Managed customers, we need to develop an algorithm to predict usage across instances who have disabled their Usage Ping.

This section explains our first attempt in these xMAU estimations.

##### Current Methodology

**What we know ?**

* The number of Active subscriptions per month
* The number of Active subscriptions that send us a Usage Ping payload per month broken down by version
* The release date (therefore month) of a specific xMAU counter in Usage ping. For example we know that the `merge_requests_users` which is the GMAU of the Create - Source Code Group was released in version 12.9

**What we don't know ?**

* Number of Active Free Instances

**What we can do ?**
We can actually find an estimation ratio which could be explained for the example of `merge_request_users`:

>>>

We can calculate every month the percentages P of subscriptions that send us a `merge_request_users` counters (so which are on the 12.9 version or a later version among all paid subscriptions). 

To go from Recorded GMAU to Estimated GMAU we could therefore do:

recorded GMAU / P

>>>


This number is actually a quite accurate number to estimate paid XMAU but can be used at first for calculating estimated xMAU. Though we can imagine several improvements in order to make this estimator more robust:

* The first improvement would be to use the sum of the seat quantity ordered instead of the number of subscriptions.
* We could also break down the estimator per plan (current problem: a subscription can have 2 instances from 2 different product tiers)
* A major area of improvement is regarding our Core estimation. We currently assume that we have the same patterns between paid and free in terms of version upgrade and usage ping opt-in. While usage ping opt-in rate estimation seems very complicated to improve an critical area of improvement would be around version upgrade and usage data trend for Core Instances

## Self-Service Data Solution

### Self-Service Dashboard Viewer

| Dashboard                                                                                                    | Purpose |
| ------------------------------------------------------------------------------------------------------------ | ------- |
| Executive Overview - TBD | This dashboard presents an executive overview of the current status of all metrics. |
| Dev Section Analysis - TBD | This dashbaord presents a section overview of the current status of related metrics. |
| [DRAFT: Centralized Dashboard for Handbook Updates](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu) | The charts on this dashboard are used for handbook embeds. |

### Self-Service Dashboard Developer

A great way to get started building charts in Sisense is to watch this 10 minute [Data Onboarding Video](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be) from Sisense. After you have built your dashboard, you will want to be able to easily find it again. Topics are a great way to organize dashboards in one place and find them easily. You can add a topic by clicking the `add to topics` icon in the top right of the dashboard. A dashboard can be added to more than one topic that it is relevant for. Some topics include Finance, Marketing, Sales, Product, Engineering, and Growth to name a few. 

#### Self-Service Dashboard Developer Certificate

`Coming Soon`

### Self-Service SQL Developer

#### Key Fields and Business Logic

`Coming Soon`

#### Self-Service SQL Developer Certificate

`Coming Soon`

#### Snippets

We built several snippets to make it easy for you to chart the different MAU/TMAU/Section MAU/SMAU/GMAU/AMAU relative to your section, stage or groups.

##### [reported_section_tmau(section)]

This snippet will retrieve Reported Total Monthly Active Users at the section level. This snippet is using usage ping as the only source of truth.

**Parameters:**

- section: the name of the section (not capitalized, so the accepted values are `dev`, ). You can also use `All` as a value for this parameter. It will then retrieve ALL sections.

**Dimensions:**

- section_name
- delivery

Example SQL Chart in Periscope (you can copy and paste this SQL reference in a New chart Editor in SiSense):

```
[reported_section_tmau('dev')]
```

[Example chart](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu?widget=9982128&udv=1146726)

##### [reported_smau(section, stage, target)]

This snippet will retrieve Reported Stage Monthly Active Users at the stage level. This snippet is using usage ping as the only source of truth.

**Parameters:**

- section: the name of the section (not capitalized, so the accepted values are `dev`, ).
- stage: the name of the section (not capitalized, so the accepted values are `dev`, ) in camel case. You can also use `All` as a value for this parameter. It will then retrieve ALL sections.
- target: to get a target line added to the charts. If you don't have a target, you can simply set this to 0 and it won't show.

**Dimensions:**

- section_name
- stage_name
- delivery

Example SQL Charts in Periscope (you can copy and paste this SQL reference in a New chart Editor in SiSense):

```
[reported_smau('dev', 'create', 10000000)]
```

And without target:

```
[reported_smau('dev', 'create', 0)]
```

[Example chart 1](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu?widget=9967642&udv=1146726)
[Example chart 2](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu?widget=9992267&udv=1146726)

##### [reported_gmau(stage, group, 0)]

This snippet will retrieve Reported Group Monthly Active Users at the group level. This snippet is using usage ping as the only source of truth.

**Parameters:**

- stage: camel-case'd name of the stage (not capitalized, so the accepted values are `dev`, ). You can also use `All` as a value for this parameter. It will then retrieve ALL sections.
- group: camel_case'd name of the group
- target: to get a target line added to the charts. If you don't have a target, you can simply set this to 0 and it won't show.


**Dimensions:**

- stage_name
- group_name
- delivery

Example SQL Charts in Periscope (you can copy and paste this SQL reference in a New chart Editor in SiSense):

```
[reported_smau('create', 'gitaly', 10000000)]
```

And without target:

```
[reported_smau('monitor', 'monitor', 0)]
```

#### Reference SQL

`Coming Soon`

#### Data Discovery Function in Sisense

If you are not familiar with SQL, there is the Data Discovery function in Sisense wherein you can create charts through a drag-and-drop interface and no SQL query is needed.

##### How to work with the Data Discovery Function

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/h_b9A8F7Ic8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

More information here on the [Data Discovery page in Sisense](https://dtdocs.sisense.com/article/data-discovery).

#### Entity Relationship Diagrams

`Coming Soon`

## Data Platform Solution

### Data Lineage

`Coming Soon`

### DBT Solution

`Coming Soon``

## Trusted Data Solution

[Trusted Data Framework](https://about.gitlab.com/handbook/business-ops/data-team/direction/trusted-data/)

### EDM Enterprise Dimensional Model Validations

`Coming Soon`

### RAW Source Data Pipeline validations

`Coming Soon`

### Manual Data Validations

* [Manual Usage Ping Validation Dashboard](https://app.periscopedata.com/app/gitlab/762611/Manual-Usage-Ping-Validation)


---
layout: markdown_page
title: "Channel Services Program"
---
## GitLab Channel Services Program

The GitLab Channel Services Program is designed to help new or existing partners design service portfolios around the DevOps Lifecycle in correlation to GitLab products. The program will help partners evaluate the business opportunities of offering services and create a technical enablement framework to use as they build their GitLab related service practices. 

The foundation of the GitLab Services Program  consists of two elements on which partners can build upon to start or grow their GitLab related DevOps service portfolio:
Certifications:  GitLab is delivering new GitLab Certified Service Partner Badges, that include training for service delivery teams which enable partners to meet Program compliance requirements. 
Service Kits:  Partners can utilize the GitLab packaged service kits to assist their teams as they build the service offerings, market and sell their services, and support their service business growth. 

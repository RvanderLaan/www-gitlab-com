---
layout: handbook-page-toc
title: "Live Doc Meetings"
canonical_path: "/company/culture/all-remote/live-doc-meetings/"
description: How to create efficient, transparent, documentation-based meetings
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Introduction

![GitLab collaboration](/images/all-remote/gitlab-collaboration.jpg){: .medium.center}

On this page, we're detailing how to create efficient, transparent, documentation-based meetings. These meetings are not exclusive to remote teams. They increase cohesion, discipline, and transparency in all meetings regardless of the work environment. 

For a deeper dive on how GitLab thinks about and implements all facets of meetings in a remote work environment, visit our [all-remote meetings guide](/company/culture/all-remote/meetings/). 

## Live doc meetings

![GitLab all-remote mentor](/images/all-remote/ceo-shadow-gitlab-awesomeness.jpg)
{: .shadow.medium.center}

Every work-related meeting should have a live doc agenda affixed to the calendar invite. To better understand how GitLab utilizes agenda docs, here's a [templated example](https://docs.google.com/document/d/1WQe-0oiMCzB3MPBNdKluCEIfgTRpaIi-SJ8FmUJ2xHo/edit?usp=sharing) you can copy and use in your organization.

If you determine that a meeting is needed to move a project forward, address a blocker, or resolve a miscommunication, be sure to have an agenda and be disciplined about using it.

1. Create the agenda in a Google Doc ahead of time (this can be any shared document service, though GitLab uses [Google Workspace](https://workspace.google.com/))
1. Ensure that the appropriate sharing settings are in place to prevent a chorus of "I can't access the agenda" comments; agendas should be editable by all participants
1. Link the agenda in the meeting invite
1. Establish the agenda as far in advance of the meeting as possible
1. Agendas should be simple: numbered, bulleted lists that are concise and direct — a [boring solution](/handbook/values/#boring-solutions).
1. Questions should be listed upfront; this tends to increase the quality of questions
1. Link any relevant piece of text to associated resources to prevent people from spending time searching or asking
1. Take notes inline with the agenda rather than using a seperate "notes" section, with real-time notetaking by multiple participants encouraged 
1. Notes should be properly indented and structured to follow the conversation's flow
1. Preface questions and answers with the participant's name, giving context to the origin
1. Follow the order of the questions instead of jumping around in the agenda; this prevents the proverbial loudest voice in the room from dominating a conversation 
1. Always ask if the name preceding the question would like to verbalize; if they wish not to, or are otherwise unavailable, the meeting host should verbalize on their behalf
1. Reenact questions in the list by verbalizing them even if they were already asked and answered in the shared document. Hearing the question and answer allows everyone to think about it and respond to it. It might also happen that when verbalizing the question or answer more context is given since most people can talk faster and with less effort than they can type. Another advantage if you're recording the meeting is that the recording contains everything.
1. Unless the meeting contains [Not Public](/handbook/communication/#not-public) information, record the meeting to share with those who cannot attend live and consider livestreaming for maximum transparency and feedback potential 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/YnSNyJue0L8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

The [GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) video embedded above shares an overview of how Live Doc Meetings are used. 

## Benefits of live doc meetings

![GitLab collaboration illustration](/images/all-remote/gitlab-collaboration-illustration.jpg){: .medium.center}

Establishing a culture where team members are cognizant that they work with others who may be in a different location, or unable to attend a meeting live, is critical. Documentation is a vital part of avoiding team [dysfunction](/handbook/values/#five-dysfunctions). 

Improving your meeting hygiene can start by shifting to live doc meetings. It is a relatively simple step, and tends to create an understanding of the value of documentation. If you're looking for a place to start in capturing teamwide buy-in on documentation, consider adding agendas to all work-related meetings and insist on live documentation during them.  

1. It prevents [knowledge leaks](/company/culture/all-remote/effective-communication/#why-text-communication-is-important-for-successful-remote-working). Meetings without agendas are only useful to those in it, and even then, it's likely that outcomes will be forgotten, or critical pieces of the puzzle will become less familiar over time.
1. It creates a more [inclusive](/company/culture/inclusion/building-diversity-and-inclusion/) meeting atmosphere. People can add questions and insights before (and even after) a synchronous meeting. Plus, those who are less comfortable verbalizing points in front of management can use the agenda doc to properly articulate their complete thoughts.
1. It creates a takeaway. The agenda doc lives longer after the meeting. If you want to share it further, perhaps sending to a person or team that you realize would have benefitted from attending, you're able to do so.
1. It shifts the memorization burden away from humans. If there's an agenda doc affixed to each calendar invite, you can easily search your calendar for key words (marketing, CEO, engineering, etc.), find a given meeting, and immediately access a documented history of what was discussed.

Not all meetings are inherently bad. We encourage managers to establish regular 1:1 meetings with their team, for example. Many meetings can be avoided by understanding how to [work well asynchronously](/company/culture/all-remote/asynchronous/). GitLab has a [documented approach to efficient, productive 1:1s](/handbook/leadership/1-1/) that we welcome other companies to implement, and contribute to if they have suggestions for improvement.

- - -

Return to the main [all-remote page](/company/culture/all-remote/).

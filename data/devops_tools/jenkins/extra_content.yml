pages:
  - path: jenkins-gaps
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Jenkins Gaps
      css: extra-content-devops-tools.css
      page_body: |
        ## On this page
        {:.no_toc}

        - TOC
        {:toc}

        ## Not a Single DevOps Application

        * Jenkins is not an end to end platform for the entire DevOps Software Lifecycle

        ## Plugins

        * Extending the native functionality of Jenkins is done through plugins. Plugins are expensive to maintain, secure, and upgrade.
            - Follow on: "Most of the organizations we talk to tell us that managing Jenkins plugins is a nightmare. Each one has it's own development life cycle, and each time a new one comes out that they need they have to re-test the whole tool chain. We've heard stories like Group A needs plugin A, and the Jenkins administrator installs it, only to find out that it depends on a newer (and incompatible) version of the library that plugin B requires, which Group B needs. Solution? Give each group their own Jenkins server? But then you end up with Jenkins sprawl (more servers to manage). Not to mention all the maintenance and testing required to all the externally integrated tools in the tool chain." (expect to see a lot of nodding during this spiel)

        ## High Maintenance

        * Jekins requires dedicated resources with Groovy Knowledge
        * Maintaining 3rd party Plug-ins can be difficult

        ## Developer Experience

        Jenkins quickly established itself as a leading build orchestration tool after its inception in 2005 and continued gaining steam before the term "DevOps" and "CI/CD" was all the craze like we hear about today. As the saying goes, "time stops for no one," and we're seeing more rapid advancements in technology and software development than ever before. Standards from 20 years ago seem foreign and what worked even 5 years ago is unlikely to still be considered best practice. We're all having to adapt quickly to stay relevant and compete in the current technological climate.
        Unfortunately for Jenkins, what worked so well 15 years ago hasn't stood the test of time and the lack of innovation to remain current and improve the project has taken a toll on its seemingly dwindling community of users and open source contributors, at least compared to how thriving the community once was. The two main Jenkins differentiators:

        * Jenkins being the "Swiss army knife of automation" with seemingly unlimited flexibility because of its plugin ecosystem

        * A very active, excited, and thriving open source community of contributors

        The above has turned into a double edged sword scenario where the plugin ecosystem has grown to a scale that's difficult to maintain and secure but still critical to making up for what Jenkins lacks in native capabilities. Jenkins itself is a java application with a severely outdated UI/UX, so problems of this nature are compounded for developers trying to use it efficiently and effectively -- a constant uphill battle. Additionally, the once thriving community has seen a consistently significant decrease in activity and adoption.

        While Jenkins is great for automating some things it is very brittle and is taking away from the developer experience. Specifically:

        * Jenkins has brittle configurations, fragile UI
        * Jenkins' outdated and unmaintained plugins continues to grow
        * Jenins is impacting Developer happiness and productivity directly impacted, leads to difficulty retaining talent for Jenkins shops

        ## Increased Risk

        * Integrationg 3rd party plug-ins creates exposure and introduces security risk
        * Jenkins upragades may cause downtime due to pluh-in integrations

        ## Enterprise Readiness

        * Jenkins requires 3rd party plug-ins to support analytics
        * With Jenkins, audit and compliance requirements are not easy to meet.  It's not easy to track changes in a new release.

        ## Jenkins X

        * Jenkins X has high system requirements, Deployment limited to Kubernetes Clusters only, Only has a command line interface (at present) and Still uses Groovy

        ## Kubernetes Support
        * From GitLab PMM - “Jenkins had to build a whole new separate project in order to work with Kubernetes. GitLab has natively adopted Kubernetes from the get-go.”
  - path: gitlab-differentiators
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: GitLab Differentiators
      css: extra-content-devops-tools.css
      page_body: |
        ## On this page
        {:.no_toc}

        - TOC
        {:toc}

        ## Single Appication
        * GitLab provides more than what Jenkins is hoping to evolve to, by providing a fully integrated single application for the entire DevOps lifecycle. More than Jenkins' goals, GitLab also provides planning, SCM, packaging, release, configuration, and monitoring (in addition to the CI/CD that Jenkins is focused on).  With Gitlab, there is no need for plugins and customization.  Unlike Jenkins, GitLab is [open core](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/) and anyone can contribute changes directly to the codebase, which once merged would be automatically tested and maintained with every change.

        ## Maintenance
        * Maintaining GitLab is quite easy to maintain and update.  To upgrade, just replace one docker image
        When you upgrade version, it upgrades everything.
        * Maintaining Pipleline Definitions are cleaner and easier than in Jenkins.  The GitLab CI/CD configuration file  (.gitlab-ci.yml):
            * Uses a structured YAML format
            * Has a shorter learning curve - simpler to get up and running
            * Prevents unconstrained complexity - which can make your Jenkins pipeline files hard to understand and manage
            * Versioned in repository next to your code - makes it easy to maintain and update by developers

        ## Optimized for Cloud Native development
        * GitLab has
            * Built-in container registry
            * Kubernetes integration
            * Microservices CI/CD logic
            * Cluster monitoring
            * Review Apps

        ## Visiblity and Intuitiveness
        * Developers have visibility to the entire development lifecycle
        * Modern UX and ease of use

        ## Better execution architecture
        * The GitLab Runner, which executes your CI/CD jobs, has an execution architecture that is more adaptable than Jenkins
            * Written in Go for portability - distributed as single binary without any other requirements.
            * Native Docker support - Jobs run in Docker containers.  GitLab Runner downloads the container images with the necessary tools, runs the jobs, then removes the container.
            * No extra dev tool pre-installs needed on the Runner machines
            * Built-in auto-scaling -  machines created on demand to match job demand. Enables cost savings by using resources more dynamically.

        ## Permission Model
        * GitLab has a Single Permission Model which makes setting roles and permissions in GitLab faser and easier.

  - path: licensing
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Jenkins Licensing
      css: extra-content-devops-tools.css
      page_body: |
        # On this page
        {:.no_toc}

        - TOC
        {:toc}


        ## Jenkins Pricing

        * Jenkins OSS
           - Free (and Open Source)
           - But Total Cost of Ownership is not zero, given maintenance requirements
        * CloudBees Jenkins
         (vague) - https://www.cloudbees.com/products/pricing
           - CloudBees Core - Jenkins distribution with upgrade assistance on monthly incremental upgrades, cloud native architecture, centralized management, 24/7 support and training, enterprise-grade security and multi-tenancy, and plugin compatibility testing
              - starting at $20k/year for 10 users, with tiered pricing for lower per-user cost for larger organizations
        * Jenkins X
          - Free (and Open Source)
          - But Total Cost of Ownership has cost (see [Pinterest anecdote](/handbook/marketing/strategic-marketing/customer-reference-program/#deliver-value-faster))

        ## Is Jenkin Really Free?
        Here are some additional cost that should be considered for Jenkins users:
           * Script development and maintenance, Jenkins will require an expert for this.
           * Infrastructure Costs
           * Additional licensing cost for Security, this is included with GitLab
           * Cost of reporting
  - path: news
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: News and Announcements
      css: extra-content-devops-tools.css
      page_body: |
        ## HackerNews Thread - [Jenkins is Getting Old](https://news.ycombinator.com/item?id=19781251)

        ## Jenkinstein - From the article [DevOps World 2018: ‘Jenkinstein’ and a Cloud Native Jenkins](https://thenewstack.io/devops-world-2018-jenkinstein-and-a-cloud-native-jenkins/)
          - Describing the snowflake server and the ["Brent"](http://devopsdictionary.com/wiki/Brent) situation and how it slows down everything. This is one of the main issues which Jenkins users face today and which Jenkins has no easy fix for:
            > Acting as the gatekeeper for that channel is someone CloudBees CEO Sacha Labourey described as “the Jenkins guy… this superstar devoted to making Jenkins great on his team. Because this person is the authority on deployment within his organization, multiple teams come to rely on him to meet their scheduling goals. Yet this leads to a technical issue that few folks outside of IT operations take time to consider: The Jenkins Master . . . (the server managing a distributed scheme with multiple agents) becomes bloated, like an old telephone directory or the Windows XP System Registry.
            >
            > And because that organization’s Jenkins deployment is not only dependent upon the Guy, but somewhat bound to his choices of plug-ins, the result is what the CEO called “Frankenstein Jenkins,” and what other developers and engineers at the conference Tuesday had dubbed “Jenkinstein.”
          - Again, CloudBees CEO Sacha Labourey describing the common problem with current Jenkins:
            >  Jenkins becomes bloated, slow to start. When it crashes, it takes forever to start. Hundreds of developers are pissed. And nobody wants to fix it, because if you touch it, you own it, right?”

        ## [From note on 2018-08-31 from CloudBees CTO and Jenkins creator Kohsuke Kawaguchi](https://jenkins.io/blog/2018/08/31/shifting-gears/):
          - "Our Challenges. . . Service instability. . . Brittle configuration. . . Assembly required. . . Reduced development velocity. . ." (see above link for details on each)
          - "Path forward. . . Cloud Native Jenkins. . . continue the incremental evolution of Jenkins 2, but in an accelerated speed"
          - **Key takeaways**:
             - They plan on BREAKING backward compatibility in their upcoming releases
             - They plan on introducing a NEW flavor of Jenkins for Cloud native
             - If you’re a Jenkins user today, it’s going to be a rough ride ahead

        ## [From Jenkins Evergreen project page (identified in Kohsuke letter above as key for changes that need to come)](https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc)
           - "Pillars . . . Automatically Updated Distribution . . . Automatic Sane Defaults. . . Connected. . . Obvious Path to User Success"
           - "The "bucket of legos" approach is . . . not productive or useful for end-users [5] who are weighing their options between running Jenkins, or using a CI-as-a-Service offering such as Travis CI or Circle CI."
           - "existing processes around "Suggested Plugins", or any others for that matter, result in many "fiefdoms" of development rather than a shared understanding of problems and solutions which should be addressed to make new, and existing, users successful with Jenkins."

        ## [From "Problem" definition page of Jenkins Evergreen project page](https://github.com/jenkinsci/jep/blob/master/jep/300/README.adoc#problem):
           > For novice-to-intermediate users, the time necessary to prepare a Jenkins environment "from scratch" into something productive for common CI/CD workloads, can span from hours to days, depending on their understanding of Jenkins and it’s related technologies. The preparation of the environment can also be very error prone and require significant on-going maintenance overhead in order to continue to stay up-to-date, secure, and productive.
           >
           > Additionally, many Jenkins users suffer from a paradox of choice [6] when it comes to deciding which plugins should be combined, in which ways, and how they should be configured, in order to construct a suitable CI/CD environment for their projects. While this is related to the problem which JEP-2 [7] attempted to address in the "Setup Wizard" introduced in Jenkins 2.0, Jenkins Evergreen aims to address the broader problem of providing users with a low-overhead, easily maintained, and solid distribution of common features (provided by a set of existing plugins) which will help the user focus on building, testing, and delivering their projects rather than maintaining Jenkins.

        ## [Project analysis on Jenkins, pointed to by CloudBees documentation as proof for need of change](https://ghc.haskell.org/trac/ghc/wiki/ContinuousIntegration#Jenkins)
           > Pros
           >   - We can run build nodes on any architecture and OS we choose to set up.
           >
           > Cons
           >   - Security is low on PR builds unless we spend further effort to sandbox builds properly. Moreover, even with sandboxing, Jenkins security record is troublesome.
           >   - Jenkins is well known to be time consuming to set up.
           >   - Additional time spent setting up servers.
           >   - Additional time spent maintaining servers.
           >   - It is unclear how easy it is to make the set up reproducible.
           >   - The set up is not forkable (a forker would need to set up their own servers).

  - path: key-questions
    content:
      header: includes/devops-tools/headers/no-button.html.haml
      page_title: Key Questions to Ask
      css: extra-content-devops-tools.css
      page_body: |
        # On this page
        {:.no_toc}

        - TOC
        {:toc}

        ## Maintenance and Scalability Concerns

        |     Concerns You May Have     |                                                                                                                     Questions To Ask                                                                                                 |
        |:----------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        |          Dedicated Resources |                                                                      How do we build and maintain pipelines, manage machines, install tools etc.?                                                                      |
        | Specialized Groovy Expertise |                                       I will need people with Groovy knowledge to write scripts in Jenkins. Who are my Groovy experts and have they been with our company long?                                        |
        |             Plug-in Security |                      Use of third party plugins to access Jenkins module will create exposure?  How will we test these plug-ins?  How will we maintain access permissions to the Jenkins modules?                      |
        |          Upgrades are Risky  | Because of a daisy chain of plug-ins and custom development, upgrades cause downtime.  How much downtown time will we have from upgrades?  How often do we upgrade Jenkins?<br>What process do we follow for upgrades? |

        ## Enterprise Readiness Concerns

        |    Concerns You May Have   |                                                                                  Questions To Ask                                                                                  |
        |:-------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        | UI using single container |                    Jenkins UI is run on a single container with no failover or clustering.  What’s our scaling and failover set up? What kind of uptime do we target?                    |
        |         Lack of Analytics |             Jenkins needs to rely on plug-ins for analytics.  What are the metrics that we track?  (Release velocity, Lead time from Issue to Deployment, Bugs per release?)             |
        |     Audits and Compliance | With Jenkins, it's not easy to track all that occurred  in a release.  If I'm in a regulated industries, what are our audit and compliance requirements? How often do we need to report? |

        ## Too Many Community PlugIns Concerns

        | Concerns You May Have |                                                                                   Questions To Ask                                                                                   |
        |:---------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        |      Too Many PlugIns |           With Jenkins it can be hard to find what you need, sometimes there are 30+ plugins for a single action, how to choose?  How many plugins do we use?  For what purpose?           |
        |   PlugIn Dependencies |              Jenkins upgrades may depend on many plugins, some may not be compatible which could slow upgrades.  How often do would we upgrade plugins and the Jenkins server?             |
        |        PlugIn Quality | Jenkins has received complaints of poor plugin quality and lack of maintenance.  Do we actively monitor our plugins and certify?  What is the process to validate new versions of plugins? |

        ## Higher OpEx Concern

        * We use Jenkins Open Source.  Moving to GitLab CI will increase our operational cost requiring a subscription and new expertise

        |                                                                                                                 True Cost                                                                                                                |
        |:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        |                                   Maintaining Jenkins often requires a minimum time commitment of 10% of a developer's times (10% of their salary dedicated to Jenkins)..remember this is for CI alone                                   |
        |                                                                GitLab CI reduces build times which contributes to delivering quality code faster which leads to lower OpEx                                                               |
        |                               GitLab’s single application offers easily consumable UI knobs and system features (We can Demo this for you!), presenting a quick and easy (i.e. affordable) learning curve.                               |
        | Deep Jenkins technical expertise will require CloudBees which is offered through a paid subscription plan.  GitLab offers a free tier and includes support within the paid tiers                                                         |
        | Plugins with Jenkins could cost you in downtime due to outdated Plugins or security vulnerability introduced after upgrading.  GitLab offers one end-to-end software delivery application that embeds enterprise grade security features |

        ## Decision Silos Concern

        * We allow each team to select their own DevOps tools.  We don’t want to force one tool across the organization.

        |                                     Cross team collaboration                                     |                                                                                                                                    |
        |:------------------------------------------------------------------------------------------------:|------------------------------------------------------------------------------------------------------------------------------------|
        | GitLab improves synergies across the company by providing a single application that accelerates: | Sharing of ideas<br>Providing feedback<br>Project discussions among different groups/teams<br>Task assignments across groups/teams |

        ## Do Nothing Concern

        * We have decided to just “Do Nothing” at this point in time

        | Cost of the Comfort Zone |                                                                                                                                                                                        |
        |:------------------------:|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
        |         OpEx Cost        | Jenkins Open Source code is free, however, Jenkins implementations are not exactly free….beginning the process today of migrating from Jenkins will reduce your <br>OpEx               |
        |       Branding Cost      | Jenkins may open your organization up to security vulnerabilities that could lead to branding vulnerabilities                                                                          |
        |     Productivity Cost    | Software delivery with Jenkins CI increased code delivery time through increased build times<br>Maintaining Jenkins is time consuming and manual (requiring manual script development) |

        ## Migration Time Concern

        * Rebuilding my Jenkins CI Pipelines in GitLab will take too much time

        |                                                                       Jenkins Wrapper                                                                       |
        |:-----------------------------------------------------------------------------------------------------------------------------------------------------------:|
        |    Building an automated translator between GitLab and Jenkins would be an ideal solution, which we have extensively evaluated and continue to evaluate.    |
        |         At the moment, we’ve determined that the best path forward in moving your CI jobs from Jenkins to GitLab is to implement a Jenkins wrapper.         |
        |                              The Wrapper will allow you to put your Jenkins CI jobs within a container and run them in GitLab.                              |
        | The Jenkins Wrapper does not allow you to experience the full benefits of GitLab CI, however, it is a positive step forward in your migration plan/strategy |

        ## Needed Expertise

        * I’m not sure my team has the expertise needed to translate our Jenkins CI jobs to GitLab

        |                                              Jenkins Migration Services                                              |
        |:--------------------------------------------------------------------------------------------------------------------:|
        | GitLab has a professional service offering to assist with migrating from legacy CI systems such as Jenkins to GitLab |
        |             The Jenkins Migration Services offers three phases:<br>Evaluation<br>Education<br>Empowerment            |

        ## Functional Parity Concern

        * We have extended the functionality of our Jenkins CI tool with Plugins.  I’m concerned that GitLab my not be able to support these added features

        |                                                                                             Native Codebase and Open Source                                                                                            |
        |:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
        |                                                                      To extend the native functionality of Jenkins you are required to use Plugins                                                                     |
        |                                             With GitLab, we do support Plugins but you will find that our core codebase supports many features offered by 3rd party Plugins                                            |
        | Furthermore, GitLab Core is Open Source which means that anyone can contribute and add new features/functionality to the codebase which will be merged with the mainline code and automatically tested and maintained. |

- name: Infrastructure Hiring Actual vs Plan
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/people-group/performance-indicators/"
  definition: Employees are in the division "Engineering" and department is "infrastructure".
  target: greater than 0.9
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Currently hiring for 2 positions (backfills for DBRE, Backend Eng).
  sisense_data:
    chart: 8637909
    dashboard: 528609
    embed: v2
  urls:
  - "/handbook/hiring/charts/infrastructure-department/"
- name: Infrastructure Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Identified in Sisense Chart
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Currently running approximately 21% of plan YTD as of Aug 30 (manually calculated
      from Sisense chart).
- name: GitLab.com Availability
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Percentage of time during which GitLab.com is fully operational and
    providing service to users within SLO parameters. Definition is available on the
    <a href="https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml">GitLab.com
    Service Level Availability page</a>.
  target: equal or greater than 99.95%
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/environments/1790496/metrics?dashboard=.gitlab%2Fdashboards%2Fsla-dashboard.yml&duration_seconds=2592000
  health:
    level: 2
    reasons:
    - Availability improved in August, although still short of our 99.95 target
    - Aug 2020 Availability 99.87%
    - FY21 Q2 Overall 90 day Availability 99.65%
    - July 2020 Availability 99.81%
    - June 2020 Availability 99.56%
    - May 2020 Availability 99.58%
    - Continuing to use the Apdex availability measure.
- name: GitLab.com Performance
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric needs to reflect the performance of GitLab as experienced
    by users. It should capture both frontend and backend performance. Even though
    the Infrastructure will be responsible for this metric they will need other departments
    such as Development, Quality, PM, and UX to positively affect change.
  target:
  org: Infrastructure Department
  is_key: false
  urls:
  - "/handbook/engineering/performance/#past-and-current-performance"
  health:
    level: 2
    reasons:
    - Most current end-user peformance work is being executed by projects in Development
      and Quality teams. Current Infrastructure focus during FY21 Q2 & Q3 is deliberately
      scoped to Reliability and Cost Efficiency efforts.
- name: Mean Time To Production (MTTP)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time (in hours) from merging a change in <a href="https://gitlab.com/gitlab-org/gitlab">gitlab-org/gitlab
    projects</a> master branch, to deploying that change to gitlab.com. It serves
    as an indicator of our speed capabilities to deploy application changes into production.
  target: less than 12 hours
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170
  health:
    level: 3
    reasons:
    - In September 2020, the target is lowered to 12 hours (previously 24).
    - The next step for this metric is to move from Continuous Delivery to Continuous Deployment for GitLab.com. Work is tracked in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280"> epic 280 </a>.
    - The target will be considered for lowering further once we achieve three consecutive months below the target AND when we introduce(and successfully use) automated rollbacks for three consecutive months.
  sisense_data:
    chart: 7355324
    dashboard: 564229
    embed: v2
- name: Infrastructure Hosting Cost vs Plan
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Tracks our actual infrastructure hosting costs against our planned infrastructure
    hosting costs for GitLab.com. We need this metric to manage our financial position.
  target: at or below Finance Plan
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/528609/WIP:-Infrastructure-KPI-Top-Level?widget=6990885&udv=836771
  - https://gitlab.com/groups/gitlab-com/-/boards/1502173?label_name[]=infrafin
  - https://about.gitlab.com/handbook/engineering/infrastructure/cost-management/infrafin-board/
  health:
    level: 3
    reasons:
    - Cost efficiency programs continuing to achieve reductions in month/month cloud
      spend increases.
    - Davis T and Andrew T have worked together in creating uniform scope and weighting process for infra fin issues (see URL link).
    - Efforts continue to actively manage Issues on the infrafin board (see link)
    - Met Plan throughout August, making additional efforts to positively impact further efficiency to the end of FY21.
- name: Infrastructure Hosting Cost per GitLab.com Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support
    one user in GitLab.com. It is an important metric because it allows us to estimate
    infrastructure costs as our user base grows. Infrastructure Hosting Cost comes
    from Netsuite; it is a sum of actual amounts with the unique account name '5026
    - Hosting Services COGS' or '6026 - Hosting Services'. This cost is divided by
    <a href="/handbook/product/metrics/#monthly-active-user-mau">MAU</a>
  target: less than .80
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/528609/WIP:-Infrastructure-KPI-Top-Level?widget=7002009&udv=836771
  health:
    level: 1
    reasons:
    - See notes in Key Agenda Doc.
- name: Infrastructure Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: equal or less than 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are close to our target, but must focus on more efficient LF sourcing for
      future headcount.
    - Two recent attritions have slightly increased the remaining ALF
    - We are currently hiring for two backfills and have additional headcount coming in H2 where we will look to further improve LF.
  sisense_data:
    chart: 6847486
    dashboard: 528609
    embed: v2
- name: GitLab.com known application scaling bottlenecks
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric needs to show how much knowledge we have over the scaling
    bottlenecks for each of the signifficant GitLab services on GitLab.com. While any
    specific service is owned by Development department, operating the service, and
    keeping it performant, at scale is in purview of the Infrastructure department.
  target: 100%
  org: Infrastructure Department
  is_key: false
  urls:
  - "/handbook/engineering/infrastructure/team/scalability/goals.html"
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/296
  health:
    level: 1
    reasons:
    - In September 2020, we have 13% of the target. We have gathered in depth knowledge of 2 important services and taking action to resolve some pressing issues. Additionally, we exposed the need to explicitly prioritise services prior to venturing service exploration.
    - This metric was introduced in August 2020, and is manually tracked. Work on this metric is tracked in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/296"> the epic gl-infra/296 </a>.
    - This metric alone will not be sufficient to describe our knowledge of the system as a whole, but is proving to be useful as an indicator of the level of detail we have for each service.
- name: Infrastructure Handbook Update Frequency
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-handbook-update-frequency"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**`
    over time.
  target: greater than 50 per month
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - Continuing to watch this metric as well as work to implement the data changes which will incorporate more of our [activity from ops.gitlab.net](https://gitlab.com/gitlab-data/analytics/-/issues/2654).
  sisense_data:
    chart: 8073971
    dashboard: 621063
    shared_dashboard: b69578ca-d4a6-4a99-b06f-423a3683446c
    embed: v2
- name: Infrastructure Discretionary Bonus Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Infrastructure department
    every month.
  target: near 10%
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - August rate dropped to 3% (1 bonus), after 3 consistent months at 8% (3 bonuses)
    - Will continue to monitor. No corrective action at this point.
#- name: Mean Time To Detection (MTTD)
#  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
#  definition: Measures the elapsed time it takes us to detect the onset of an anomalous
#    condition and its actual detection, and serves as an indicator of our ability
#    to monitor the environment and minimize incident resolution.
#  target: TBD
#  org: Infrastructure Department
#  is_key: false
#  urls:
#  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8090
#  health:
#    level: 0
#    reasons:
#    - We can’t properly evaluate the KPI at this time due to inconsistent data.
#    - Q3 OKR will fix this data issue.
#- name: Mean Time To Resolution (MTTR)
#  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
#  definition: Measures the elapsed time in hours it takes us to recover when an incident
#    occurs, and serves as an indicator of our ability to execute said recoveries.
#    (Only includes severity::1 & severity::2 incidents)
#  target: less than 1 hour
#  org: Infrastructure Department
#  is_key: false
#  urls:
#  - https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1717012?&label_name[]=incident
#  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8296
#  health:
#    level: 0
#    reasons:
#    - Data is not accurate as of May when a switch to using a workflow for incidents
#      leveraging scoped labels was made.
#    - Q3 OKR will fix this data issue
#- name: Disaster Recovery (DR) Time-to-Recover
#  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
#  definition: Tracks time to recover full operational status in case of a catastrophic
#    incident in our primary production environment.
#  target: TBD
#  org: Infrastructure Department
#  is_key: false
#  urls:
#  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8293
#  health:
#    level: 0
#    reasons:
#    - Planning to complete an effort in Q3 to establish the proposal for augmenting
#      various aspects of DR in FY22.
- name: Infrastructure Department Narrow MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Infrastructure Department <a href="/handbook/engineering/#merge-request-rate">MR
    Rate</a> is a performance indicator showing how many changes the Infrastructure
    team implements in the GitLab product, as well as changes in support of the GitLab
    SaaS infrastructure.  We currently count all members of the Infrastructure Department
    (Directors, Managers, ICs) in the denominator, because this is a team effort.
    The <a href="/handbook/engineering/merge-request-rate/#projects-that-are-part-of-the-product">projects that are part of the product</a> contributes to the overall product development efforts.
  target: Greater than TBD MRs per month
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - We have known projects which are not currently included in the measure. [Working with the Data team to resolve this](https://gitlab.com/gitlab-data/analytics/-/issues/2654).
    - We intend to observe this metric over time to understand the behavior of the
      differing work patterns and MR content (product code + infrastructure logic
      + configuration data).
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4445
  sisense_data:
    chart: 8934544
    dashboard: 686934
    shared_dashboard: 178a0fbc-42a9-4181-956d-a402151cf5d8
    embed: v2
- name: Infrastructure Department New Hire Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - The goal has fluxuated above and below the target line, which at the numbers
      involved implies we're right around target
    - Current hiring for 2 backfills as well as new headcount in 2H will provide new opportunity to influence this metric.
  sisense_data:
    chart: 9389296
    dashboard: 719549
    embed: v2
- name: Infrastructure Department Corrective Action SLO
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Corrective Actions (CAs) SLO focuses on the number of severity::1/severity::2 Corrective Action Issues pass their due date. All Corrective Actions are in scope, regardless of department or project. CAs and their due dates are defined in
   <a href="/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership">here</a>.
  target: 0
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - We started tracking this in July 2020. Most of our Corrective Actions do not have a proper Severity assigned. We are working with all groups to improve this throughout Q3.
    - Current CA SLO performance is TBD
  urls:
    - https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership
